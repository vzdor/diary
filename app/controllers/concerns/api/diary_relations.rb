module Api::DiaryRelations
  extend ActiveSupport::Concern

  included do
    before_action :sign_in
  end

  def current_diary
    @current_diary ||= signed_in_user.diaries.find(params[:diary_id])
  end
end
