class Api::DiariesController < ApplicationController
  before_action :sign_in

  def index
    diaries = signed_in_user.diaries.order(id: :asc)
    render json: DiariesJsonSerializer.new(diaries)
  end

  def create
    diary = signed_in_user.diaries.build(diary_params)
    if diary.save
      render json: DiaryJsonSerializer.new(diary), status: 201
    else
      render json: {errors: diary.errors}, status: 400
    end
  end

  protected

  def diary_params
    params.require(:data).require(:attributes)
      .permit(:name, :local_id, :hide_listing)
  end
end
