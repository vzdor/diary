require 'base64'

class Api::ZqueriesController < ApplicationController
  include Api::DiaryRelations

  def all
    results = query(current_diary)
    render json: ZresultsJsonSerializer.new(results)
  end

  protected

  class Result < Struct.new(:id, :local_id, :tokens)
  end

  def query(diary)
    sql =<<EOT
SELECT id, local_id, #{aliases} FROM (
  SELECT id, local_id, #{functions} FROM diary_entries WHERE diary_id = #{diary.id}
) AS x
WHERE #{conditions}
EOT
    rows = DiaryEntry.connection.select_all(sql).rows
    rows.map { |row| Result.new(row.shift, row.shift, row) }
  end

  def trapdoors
    params[:trapdoors]
  end

  def aliases
    trapdoors.each_index.map { |i| "t#{i}" }.join(', ')
  end

  def functions
    trapdoors.each_with_index.map do |trapdoor, i|
      "#{filter_function(trapdoor)} AS t#{i}"
    end.join(",\n")
  end

  def conditions
    trapdoors.each_index.map { |i| "t#{i} = TRUE" }.join(' OR ')
  end

  def filter_function(trapdoor)
    plain = trapdoor.map { |x| Base64.strict_decode64(x) }
    array = plain.map do |x|
      hex = x.each_byte.map { |byte| '%02x' % byte }.join
      "E'\\\\x#{hex}'::bytea"
    end
    s = array.join(', ')
    "zidx_includes(local_id, fingerprints, array[#{s}])"
  end
end
