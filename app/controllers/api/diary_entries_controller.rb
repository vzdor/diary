class Api::DiaryEntriesController < ApplicationController
  include Api::DiaryRelations

  def index
    relation = current_diary.diary_entries
    entries = if params[:local_id].is_a?(String)
                from_local_ids relation
              else
                paginate relation.order(created_at: :desc)
              end
    render json: EntriesJsonSerializer.new(entries)
  end

  def show
    entry = current_diary.diary_entries.find(params[:id])
    render json: EntryJsonSerializer.new(entry)
  end

  def create
    entry = current_diary.diary_entries.build
    save_entry entry
  end

  def update
    entry = current_diary.diary_entries.find(params[:id])
    save_entry entry
  end

  def destroy
    entry = current_diary.diary_entries.find(params[:id])
    entry.destroy
    render json: {}, status: 200
  end

  protected

  def save_entry(entry)
    service = SaveEntryService.new(entry, entry_params)
    unless service.save
      render json: {errors: service.errors}, status: 400
    else
      response.headers['Location'] = api_diary_entry_path(current_diary, service.entry)
      render json: NewEntryJsonSerializer.new(service), status: 201
    end
  end

  def from_local_ids(entries)
    ary = params[:local_id].split(',')
    raise ApiError, {message: 'localId array length should be less than 100.'} if ary.size > 100
    entries.where(local_id: ary)
  end

  # Returns paginated collection with pagination starting at 0.
  def paginate(relation)
    per_page = (params[:per_page] || 10).to_i
    raise ApiError, {message: 'perPage should be less than 100.'} if per_page > 100
    relation.page(params[:page].to_i + 1).per(per_page)
  end

  def entry_params
    params
      .require(:data)
      .require(:attributes)
      .permit(:message, :local_id, :index)
  end
end
