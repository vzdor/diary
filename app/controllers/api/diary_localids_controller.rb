class Api::DiaryLocalidsController < ApplicationController
  include Api::DiaryRelations

  def create
    render json: {
             data: {
               id: current_diary.next_localid,
               type: 'localids'
             }
           }, status: 201
  end
end
