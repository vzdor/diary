class Api::RegistrationsController < ApplicationController
  def create
    user = User.new user_params
    user.diaries.build diaries_params
    if user.save
      render json: RegistrationJsonSerializer.new(user), status: 201
    else
      render json: {errors: user.errors}, status: 400
    end
  end

  private

  def user_params
    params.require(:data)
      .require(:attributes)
      .require(:user)
      .permit(:email, :password)
  end

  def diaries_params
    params.require(:data)
      .require(:attributes)
      .permit(diaries: [:name, :local_id, :hide_listing])
      .require(:diaries)
  end
end
