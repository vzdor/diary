class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods

  class ApiError < Exception
    attr_reader :status, :message

    def initialize(status: 401, message: nil)
      @status = status
      @message = message
    end
  end

  class AuthenticationError < ApiError
  end

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  rescue_from ApiError, with: :render_api_error

  private

  def render_not_found
    render json: {
             errors: {
               id: params[:id],
               status: 404
             }
           }, status: 404
  end

  def render_api_error(error)
    render json: {
      errors: {
        status: error.status,
        message: error.message
      }
    }, status: error.status
  end

  # http://api.rubyonrails.org/classes/ActionDispatch/Http/Parameters.html#method-i-parameters
  # https://github.com/rails/rails/blob/5-1-stable/actionpack/lib/action_controller/metal/strong_parameters.rb#L1059
  def params
    @_params ||= ::ActionController::Parameters.new(underscore_parameters)
  end

  # Return camelCase request parameters converted to camel_case.
  def underscore_parameters
    request.parameters.deep_transform_keys(&:underscore)
  end

  def sign_in
    @signed_in_user = authenticate_with_http_basic do |email, password|
      User.authenticate(email, password)
    end
    raise AuthenticationError unless @signed_in_user
  end

  def signed_in_user
    @signed_in_user
  end
end
