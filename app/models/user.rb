require 'user_crypto'

class User < ApplicationRecord
  has_many :diaries

  attr_accessor :password

  before_create :signup

  validates :email, presence: true
  validates :email, uniqueness: true

  # Returns the user.
  # Returns nil if password did not match.
  def self.authenticate(email, password)
    # No timeing attack protection becasue password is a key
    # derived from human-password. Read about password generation in
    # lib/user_crypto.rb.
    user = find_by_email email
    return unless user
    user if UserCrypto.user_password?(user, password)
  end

  protected

  def signup
    UserCrypto.signup self
  end
end
