class DiaryEntry < ApplicationRecord
  belongs_to :diary

  validates :message, presence: true, length: {minimum: 1}
  validates :local_id, presence: true
  validates :local_id, uniqueness: {scope: :diary_id}
  validates :fingerprints, presence: true
end
