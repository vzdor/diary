class Diary < ApplicationRecord
  belongs_to :user

  has_many :diary_entries

  validates :name, presence: true, length: {minimum: 1}
  validates :local_id, presence: true
  validates :local_id, uniqueness: {scope: :user_id}

  after_create :create_localids_sequence

  after_destroy :destroy_localids_sequence

  def next_localid
    result = Diary.find_by_sql(["SELECT nextval(?) AS val", localids_seq]).first
    result.val
  end

  protected

  def localids_seq
    "diary_#{id}_localids"
  end

  def create_localids_sequence
    self.class.connection.execute "CREATE SEQUENCE #{localids_seq}"
  end

  def destroy_localids_sequence
    self.class.connection.execute "DROP SEQUENCE #{localids_seq}"
  end
end
