class EntryJsonSerializer < JsonSerializer
  def data(options = {})
    {
      type: 'entries',
      id: @object.id,
      attributes: attributes(options)
    }
  end

  def attributes(options = {})
    attrs = {
      localId: @object.local_id,
      createdAt: @object.created_at.to_s(:short),
      updatedAt: @object.updated_at.to_s(:short),
    }
    attrs[:message] = @object.message unless options[:message] == false
    attrs
  end
end
