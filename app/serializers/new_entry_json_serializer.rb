class NewEntryJsonSerializer < JsonSerializer
  def data
    attrs = EntryJsonSerializer.new(@object.entry).data(message: false)
    # If I want to add something to the attributes
    # attrs[:attributes][:indexes] = ...
    # While with jbuilder, I will have to split into small partials
    # which is confusing.
    attrs
  end
end
