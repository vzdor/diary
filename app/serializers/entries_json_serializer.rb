class EntriesJsonSerializer < JsonSerializer
  def meta(options = {})
    attrs = {}
    if @object.respond_to?(:total_count)
      attrs[:totalCount] = @object.total_count
    end
    attrs
  end

  def data(options = {})
    @object.map { |entry| EntryJsonSerializer.new(entry).data }
  end
end
