class DiaryJsonSerializer < JsonSerializer
  def data(options = {})
    {
      type: 'diaries',
      id: @object.id,
      attributes: attributes()
    }
  end

  def attributes()
    {
      name: @object.name,
      hideListing: @object.hide_listing
    }
  end
end
