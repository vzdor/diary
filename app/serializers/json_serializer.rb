class JsonSerializer
  def initialize(object)
    @object = object
  end

  def data(options = {})
  end

  def meta(options = {})
  end

  def as_json(options = {})
    attrs = {data: data}
    meta_attrs = meta options
    attrs[:meta] = meta_attrs if meta_attrs
    attrs
  end
end
