class DiariesJsonSerializer < JsonSerializer
  def data(options = {})
    @object.map { |diary| datum(diary) }
  end

  protected

  def datum(diary)
    DiaryJsonSerializer.new(diary).data
  end
end
