class ZresultsJsonSerializer < JsonSerializer

  def data
    @object.map { |result| datum(result) }
  end

  protected

  def datum(result)
    {
      type: 'zresults',
      id: result.local_id,
      attributes: {
        modelId: result.id,
        tokens: result.tokens,
      }
    }
  end
end
