class RegistrationJsonSerializer < JsonSerializer
  def data(options = {})
    {
      type: 'registrations',
      attributes: {
        diaries: diaries_attributes
      }
    }
  end

  def diaries_attributes
    @object.diaries.map do |diary|
      attrs = DiaryJsonSerializer.new(diary).attributes
      attrs['id'] = diary.id
      attrs
    end
  end
end
