class SaveEntryService
  attr_reader :entry

  def initialize(entry, attrs)
    @entry = entry
    @attrs = attrs
    @index_attr = @attrs.delete(:index)
  end

  def save
    @entry.attributes = @attrs
    set_fingerprints if @index_attr.kind_of?(String)
    @entry.save
  end

  def errors
    @entry.errors
  end

  protected

  def set_fingerprints
    @entry.fingerprints = @index_attr.split(',', -1).map do |s|
      unsigned_to_signed Integer(s) unless s.empty?
    end
  end

  # Postgresql does not have unsigned integer type.
  def unsigned_to_signed(n)
    if n >= 32768
      -32768 + (n & 0x7fff)
    else
      n
    end
  end
end
