ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require 'minitest/spec'

# FactoryGirl configuration
FactoryGirl.definition_file_paths = [File.join(Rails.root, 'test/factories')]
FactoryGirl.find_definitions

# Is not readable without the SpecReporter.
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

# Require testing helper modules
Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all

  # Add more helper methods to be used by all tests here...
end
