require 'test_helper'

class DiaryEntryTest < ActiveSupport::TestCase
  include SpecDsl

  it 'saves' do
    entry = FactoryGirl.build(:diary_entry)
    assert entry.save
  end

  describe 'when local_id is blank' do
    it 'returns validation error' do
      diary = FactoryGirl.create(:diary)
      entry = FactoryGirl.build(:diary_entry, diary: diary)
      entry.local_id = nil
      assert !entry.save
      assert entry.errors[:local_id].present?
    end
  end

  describe 'when diary_id and local_id is not unique' do
    it 'returns validation error' do
      diary = FactoryGirl.create(:diary)
      entry = FactoryGirl.create(:diary_entry, diary: diary)
      entry2 = FactoryGirl.build(:diary_entry, diary: diary)
      entry2.local_id = entry.local_id
      assert !entry2.save
      assert entry2.errors[:local_id].present?
    end
  end

  describe 'when same local_id but different diary_id' do
    it 'saves' do
      entry = FactoryGirl.create(:diary_entry)
      entry2 = FactoryGirl.build(:diary_entry)
      entry2.local_id = entry.local_id
      assert entry2.save
    end
  end
end
