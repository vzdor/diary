require 'test_helper'

class UserTest < ActiveSupport::TestCase
  include SpecDsl

  describe 'validations' do
    it 'validates presence of email'

    it 'validates uniqueness of email'
  end

  describe '::authenticate' do
    let(:user) { FactoryGirl.create :user, password: 'test' }

    it 'authenticates by email and password' do
      assert_equal user, User.authenticate(user.email, 'test')
    end

    it 'returns nil when wrong password' do
      assert_not User.authenticate(user.email, 't')
    end

    it 'returns nil when wrong email' do
      assert_not User.authenticate(user.email + '1', 'test')
    end
  end
end
