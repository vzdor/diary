require 'test_helper'

class DiaryTest < ActiveSupport::TestCase
  include SpecDsl

  it 'saves' do
    diary = FactoryGirl.build(:diary)
    assert(diary.save)
  end

  it 'validates presence of local_id' do
    diary = FactoryGirl.build(:diary)
    diary.local_id = nil
    assert_not diary.valid?
  end

  it 'validates uniqueness of local_id' do
    user = FactoryGirl.create :user
    diary = FactoryGirl.create :diary, user: user
    invalid_diary = FactoryGirl.build :diary, user: user
    invalid_diary.local_id = diary.local_id
    assert_not invalid_diary.valid?
  end

  describe '#next_localid' do
    it 'returns next localid' do
      diary = FactoryGirl.create(:diary)
      result = Diary.find_by_sql("SELECT nextval('diary_#{diary.id}_localids') AS val").first
      assert_equal result.val + 1, diary.next_localid
    end
  end

  it 'creates localids sequence' do
    diary = FactoryGirl.create(:diary)
    assert_equal 1, diary.next_localid
  end

  it 'destroys localid sequence' do
    diary = FactoryGirl.create(:diary)
    diary.destroy
    assert_raises ActiveRecord::StatementInvalid do
      diary.next_localid
    end
  end
end
