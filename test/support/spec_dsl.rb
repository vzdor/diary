# In the Rails tests, we inherit from a *Test class.
# To make it clear how we got spec methods in a *Test class,
# include the module individually.
module SpecDsl
  def self.included(klass)
    klass.extend Minitest::Spec::DSL
    # class TrivialControllerTest < ActionDispatch::IntegrationTest
    #   describe '#index' do
    #     ...
    # The above will add '#index' to the describe_stack, but will not add
    # the TrivialControllerTest to the stack and the test result will look:
    #  '#index' ... failed. (without the class name)
    # So, add class name to the stack:
    klass.describe_stack.push(klass)
  end
end
