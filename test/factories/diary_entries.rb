FactoryGirl.define do
  factory :diary_entry do
    message 'hello'
    diary
    local_id { diary.next_localid }
    fingerprints [1]
  end
end
