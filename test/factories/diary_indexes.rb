FactoryGirl.define do
  factory :diary_index do
    sequence(:local_id) { |n| "hello#{n}" }
    token 'hello'
    entry_local_ids [1]
    diary
  end
end
