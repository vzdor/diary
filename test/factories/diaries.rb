FactoryGirl.define do
  factory :diary do
    name 'encrypted name'
    user
    sequence(:local_id) { |n| n.to_s }
    hide_listing 'encryted hide_listing'
  end
end
