FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "bob#{n}@diary" }
    password 'hello'
  end
end
