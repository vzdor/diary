require 'test_helper'

require 'openssl'
require 'base64'

require 'user_crypto'

class UserCryptoTest < ActiveSupport::TestCase
  include SpecDsl

  describe 'hmac' do
    it 'returns hmac sha-256 message' do
      key = Base64.strict_decode64 '505nT7v2taqhjyWdU5uNaqGi3+n+88R829WyfjB+2qg='
      text = UserCrypto.hmac key, 'hello'
      encoded_text = Base64.strict_encode64 text
      assert_equal 'XEDWj7ivje/0f48L7PN7WSpjc3WaqP501eS5RWjMAFk=', encoded_text
    end
  end

  describe 'signup' do
    class MyCrypto < UserCrypto
      def self.new_salt
        '1234'
      end
    end

    it 'sets user.salt' do
      user = FactoryGirl.build :user
      MyCrypto.signup user
      encoded_salt = Base64.strict_encode64 MyCrypto.new_salt
      assert_equal encoded_salt, user.salt
    end

    it 'sets salted_password using salt as hmac key' do
      user = FactoryGirl.build :user
      MyCrypto.signup user
      my_password = MyCrypto.hmac(MyCrypto.new_salt, user.password)
      my_encoded_password = Base64.strict_encode64 my_password
      assert_equal my_encoded_password, user.salted_password
    end
  end

  describe 'user_password?' do
    it 'returns true for right password' do
      user = FactoryGirl.build :user, password: 'hello'
      UserCrypto.signup user
      assert UserCrypto.user_password?(user, 'hello')
    end

    it 'returns false with incorrect password' do
      user = FactoryGirl.build :user, password: 'hello'
      UserCrypto.signup user
      assert_not UserCrypto.user_password?(user, 'blah')
    end
  end
end
