require 'test_helper'
require_relative 'api/api_spec'

require 'base64'

class SimplesController < ApplicationController
  before_action :sign_in

  def show
    render json: signed_in_user.id
  end

  def not_found
    raise ActiveRecord::RecordNotFound
  end

  def api_error
    raise ApiError.new status: 499, message: 'test'
  end
end

class SimplesControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  before do
    Rails.application.routes.draw do
      resources :simples do
        get :not_found, on: :collection
        get :api_error, on: :collection
      end
    end
  end

  after do
    Rails.application.reload_routes!
  end

  describe '#not_found' do
    let(:user) { FactoryGirl.create :user }

    before do
      get_as user, not_found_simples_path
    end

    it 'returns 404 status' do
      assert_equal 404, status
    end

    it 'includes errors' do
      assert_kind_of Hash, response.parsed_body
      assert_includes response.parsed_body, 'errors'
    end
  end

  describe '#api_error' do
    let(:user) { FactoryGirl.create :user }

    before do
      get_as user, api_error_simples_path
    end

    it 'returns status' do
      assert_equal 499, status
    end

    it 'returns message' do
      assert_equal 'test', response.parsed_body['errors']['message']
    end
  end

  describe '#show' do
    let(:user) { FactoryGirl.create :user }

    it 'renders current_user' do
      get_as user, simple_path(1)
      assert_equal user.id, response.parsed_body
    end

    describe 'with incorrect password' do
      before do
        s = Base64.strict_encode64("#{user.email}:1")
        get simple_path(id: 1), headers: {'Authorization' => "Basic #{s}"}
      end

      it 'returns 401 status' do
        assert_equal 401, status
      end

      it 'includes errors' do
        assert_kind_of Hash, response.parsed_body
        assert_includes response.parsed_body, 'errors'
      end
    end
  end
end
