require 'test_helper'
require_relative 'api_spec'

class Api::DiariesControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  let(:user) { FactoryGirl.create :user }

  describe '#index' do
    def index
      api_get api_diaries_path, params: {format: :json}
    end

    before do
      FactoryGirl.create :diary, user: user
      FactoryGirl.create :diary, user: user
      FactoryGirl.create :diary
    end

    it_requires_sign_in :index

    describe 'when signed in' do
      include SimpleJsonapi::IndexSpec

      before do
        api_sign_in user
        index
      end

      it 'returns diaries sorted by id' do
        assert_equal 2, data.size
        diary = user.diaries.first
        d0 = data[0]
        assert_equal diary.id, d0['id']
        assert_equal 'diaries', d0['type']
        assert_equal diary.name, d0['attributes']['name']
        assert_equal diary.hide_listing, d0['attributes']['hideListing']
      end
    end
  end

  describe '#create' do
    def params
      {
        'data' => {
          'type' => 'diaries',
          'attributes' => {
            'name' => 'My name',
            'localId' => '1',
            'hideListing' => '1'
          }
        }
      }
    end

    def create(options = {})
      defaults = {
        params: params,
        as: :json
      }
      api_post api_diaries_path, defaults.deep_merge(options)
    end

    it_requires_sign_in :create

    describe 'when signed in' do
      before do
        api_sign_in user
      end

      it 'returns with 201 status code' do
        create
        assert_equal 201, status
      end

      it 'adds user diary' do
        assert_difference('user.diaries.count', 1) { create }
      end

      it 'saves all attributes' do
        create
        diary = Diary.last
        assert_equal 'My name', diary.name
        assert_equal '1', diary.local_id
        assert_equal '1', diary.hide_listing
      end

      describe 'with error' do
        def invalid
          {
            'data' => {
              'type' => 'diaries',
              'attributes' => {
                'name' => '',
                'local_id' => 1
              }
            }
          }
        end

        before do
          create params: invalid
        end

        include SimpleJsonapi::ErrorsSpec
      end
    end
  end
end
