module SimpleJsonapi
  extend Minitest::Spec::DSL

  def data
    response.parsed_body['data']
  end

  module IndexSpec
    extend Minitest::Spec::DSL

    it 'returns ok' do
      assert_response :success
    end

    it 'includes data array' do
      assert_kind_of Array, data
    end
  end

  module ShowSpec
    extend Minitest::Spec::DSL

    it 'returns ok' do
      assert_response :success
    end

    it 'includes data' do
      assert_kind_of Hash, data
    end
  end

  module ErrorsSpec
    extend Minitest::Spec::DSL

    it 'returns 400 status code' do
      assert_equal 400, status
    end

    it 'includes errors' do
      assert_includes response.parsed_body, 'errors'
    end
  end
end
