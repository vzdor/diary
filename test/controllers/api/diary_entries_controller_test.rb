require 'test_helper'
require_relative 'api_spec'

class Api::DiaryEntriesControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  def jsonapi_entry(entry)
    {
      'type' => 'entries',
      'id' => entry.id,
      'attributes' => {
        'localId' => entry.local_id,
        'createdAt' => entry.created_at.to_s(:short),
        'updatedAt' => entry.updated_at.to_s(:short),
        'message' => entry.message,
      }
    }
  end


  let(:diary) { FactoryGirl.create :diary }

  let(:user) { diary.user }

  def entry_index
    [1, 2, 3]
  end

  def serialize_index(ary = entry_index)
    ary.join(',')
  end

  describe '#index' do
    attr_reader :entries

    before do
      @entries = 2.times.map do
        FactoryGirl.create(:diary_entry, diary: diary)
      end
    end

    def index(options = {})
      defaults = {params: {format: :json}}
      api_get api_diary_entries_path(diary), defaults.deep_merge(options)
    end

    it_requires_sign_in :index

    describe 'when signed in' do
      before do
        api_sign_in user
      end

      describe 'with json reply' do
        include SimpleJsonapi::IndexSpec

        before do
          index
        end

        it 'returns data element in jsonapi format' do
          assert_equal jsonapi_entry(entries.last), data[0]
        end

        it 'orders by created_at' do
          assert_equal entries.last.id, data[0]['id']
        end

        it 'returns meta' do
          meta = response.parsed_body['meta']
          assert_equal({'totalCount' => 2}, meta)
        end
      end

      it 'returns per_page number of entries' do
        index params: {per_page: 1}
        assert_equal 1, data.size
      end

      it 'returns error if per_page > 100' do
        index params: {per_page: 101}
        assert_equal 401, status
        assert_includes response.parsed_body, 'errors'
        assert_equal 'perPage should be less than 100.', response.parsed_body['errors']['message']
      end

      it 'starts pagination at 0' do
        index params: {page: 0, per_page: 1}
        page0_id = data.first['id']
        index params: {page: 1, per_page: 1}
        assert_not_equal page0_id, data.first['id']
      end

      describe 'with comma separated local_id' do
        def local_ids(ary)
          ary.join(',')
        end

        it 'returns records array' do
          ids = entries.map(&:local_id)
          index params: {local_id: local_ids(ids)}
          assert_equal data.map { |e| e['attributes']['localId'] }, ids
        end

        describe 'with too many local_ids' do
          it 'returns error' do
            index params: {local_id: local_ids(Array.new(101, 1))}
            assert_equal 401, status
            assert response.parsed_body['errors']['message']
          end
        end
      end
    end
  end

  describe '#show' do
    let(:entry) { FactoryGirl.create(:diary_entry, diary: diary) }

    def show
      api_get api_diary_entry_path(diary, entry.id), params: {format: :json}
    end

    it_requires_sign_in :show

    describe 'with json reply' do
      include SimpleJsonapi::ShowSpec

      before do
        api_sign_in user
        show
      end

      it 'returns data in jsonapi format' do
        assert_equal jsonapi_entry(entry), data
      end
    end
  end

  describe '#create' do
    def params
      {
        'data' => {
          'type' => 'entries',
          'attributes' => {
            'message' => 'Hello world',
            'local_id' => 1,
            'index' => serialize_index
          }
        }
      }
    end

    def create(options = {})
      defaults = {
        params: params,
        as: :json
      }
      api_post api_diary_entries_path(diary), defaults.deep_merge(options)
    end

    it_requires_sign_in :create

    describe 'when signed in' do
      before do
        api_sign_in user
      end

      it 'returns with 201 status code' do
        create
        assert_equal 201, status
      end

      it 'adds entry' do
        assert_difference('diary.diary_entries.count', 1) { create }
      end

      it 'sets location header' do
        create
        location = response.headers['Location']
        entry = diary.diary_entries.last
        assert_equal location, api_diary_entry_path(diary, entry)
      end

      it 'sets fingerprints' do
        create
        entry = diary.diary_entries.last
        assert_equal entry_index, entry.fingerprints
      end

      it 'converts unsigned to signed integer fingerprints' do
        create params: {
                 'data' => {
                   'attributes' => {
                     'index' => serialize_index([1, 57304])
                   }
                 }
               }
        entry = diary.diary_entries.last
        assert_equal [1, -8232], entry.fingerprints
      end

      it 'saves null fingerprints' do
        create params: {
                 'data' => {
                   'attributes' => {
                     'index' => serialize_index([1, nil])
                   }
                 }
               }
        entry = diary.diary_entries.last
        assert_equal [1, nil], entry.fingerprints
      end

      describe 'with camelcase localId' do
        def params
          {
            'data' => {
              'type' => 'entries',
              'attributes' => {
                'message' => 'Hello world',
                'localId' => 1,
                'index' => serialize_index
              }
            }
          }
        end

        it 'returns ok' do
          create
          assert_equal 201, status
        end
      end

      describe 'with invalid entry params' do
        def params
          {
            'data' => {
              'type' => 'entries',
              'attributes' => {
                'message' => '',
                'local_id' => 1,
                'index' => serialize_index
              }
            }
          }
        end

        describe 'validation errors' do
          include SimpleJsonapi::ErrorsSpec

          before do
            create
          end

          it 'includes an error on message' do
            errors = response.parsed_body['errors']
            assert errors['message']
          end
        end
      end
    end
  end

  describe '#update' do
    let(:entry) { FactoryGirl.create :diary_entry, diary: diary }

    def params
      {
        'data' => {
          'type' => 'entries',
          'attributes' => {
            'message' => 'Hello world',
            'local_id' => 1,
            'index' => serialize_index
          }
        }
      }
    end

    def update(options = {})
      defaults = {
        params: params,
        as: :json
      }
      api_put api_diary_entry_path(diary, entry), defaults.deep_merge(options)
    end

    it_requires_sign_in :update

    describe 'with sign in' do
      before do
        api_sign_in user
      end

      it 'updates entry' do
        assert_changes 'entry.reload.message' do
          update
        end
      end

      it 'returns attributes' do
        update
        assert data
        assert_includes data, 'id'
        assert_not_includes data['attributes'], 'message'
      end
    end
  end

  describe '#destroy' do
    attr_reader :entry

    before do
      @entry = FactoryGirl.create :diary_entry, diary: diary
    end

    def destroy
      api_delete api_diary_entry_path(diary, entry)
    end

    it_requires_sign_in :destroy

    describe 'with sign in' do
      before do
        api_sign_in user
      end

      it 'returns 200 status' do
        destroy
        assert_equal 200, status
      end

      it 'deletes entry' do
        assert_changes 'diary.diary_entries.count' do
          destroy
        end
      end
    end
  end
end
