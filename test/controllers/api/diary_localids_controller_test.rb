require 'test_helper'
require_relative 'api_spec'

class Api::DiaryLocalidsControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  let(:diary) { FactoryGirl.create :diary }

  let(:user) { diary.user }

  describe '#create' do
    def params
      {
        'data' => {
          'type' => 'localids'
        }
      }
    end

    def create
      api_post api_diary_localids_path(diary), params: params, as: :json
    end

    it_requires_sign_in :create

    describe 'with sign in' do
      before do
        api_sign_in user
      end

      it 'returns with 201 status code' do
        create
        assert_equal 201, status
      end

      it 'returns new localid' do
        local_id = diary.next_localid
        create
        assert_equal local_id + 1, data['id']
        assert_equal 'localids', data['type']
      end
    end
  end
end
