require 'test_helper'
require_relative 'api_spec'

class Api::ZqueriesControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  let(:diary) { FactoryGirl.create :diary }

  let(:user) { diary.user }

  def fingerprints1 # id=1, happy new year
    [18945, 467, -16128]
  end

  def fingerprints2 # id=2, hello world
    [4242, -23697]
  end

  def trapdoors1
    [
      ['VOKXNEFMh8DSMXPe1DywpjJndtVNG6uosmQwJwEv+SA='], # happy
      ['KtxQlqFVrplkj/5RE9d63jp+u8Y3jnrd+impI8pc//c='], # new
      ['Ft+PpKtWQm/ilLwQcx4v19JVqJC2e03+uIuBc3h9ol0=']  # year
    ]
  end

  def trapdoors2
    [
      ['r0Hp88SeZSqy7lZq+qpXNl/0A5IF5OliZb03IsCml0U='], # hello
      ['jMT8+TsKejZ0i9CLHsv60SMNF8HHY6YE1/JLUds1UXk=']  # world
    ]
  end

  describe 'POST all' do
    attr_reader :e1, :e2

    before do
      @e1 = FactoryGirl.create :diary_entry, diary: diary, local_id: 1, fingerprints: fingerprints1
      @e2 = FactoryGirl.create :diary_entry, diary: diary, local_id: 2, fingerprints: fingerprints2
    end

    def find
      # Looking for "happy, new, hello"
      api_post all_api_diary_zqueries_path(diary), params: {trapdoors: [trapdoors1[0], trapdoors1[1], trapdoors2[0]]}, as: :json
    end

    it_requires_sign_in :find

    describe 'with sign in' do
      before do
        api_sign_in user
        find
      end

      include SimpleJsonapi::IndexSpec

      it 'returns entry local ids with bloolean trapdoor flags as tokens' do
        assert_equal 2, data.size
        d1, d2 = data[0], data[1]
        assert_equal e1.local_id, d1['id']
        assert_equal e2.local_id, d2['id']
        assert_equal e1.id, d1['attributes']['modelId']
        # happy, new
        assert_equal [true, true, false], d1['attributes']['tokens']
        # hello
        assert_equal [false, false, true], d2['attributes']['tokens']
      end
    end
  end
end
