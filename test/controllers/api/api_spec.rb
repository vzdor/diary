require_relative 'simple_jsonapi'

require 'base64'

module SignIn
  def get_as(user, path, **opts)
    _api_do user, 'get', path, **opts
  end

  def post_as(user, path, **opts)
    _api_do user, 'post', path, **opts
  end

  def api_sign_in(user)
    @_api_user = user
  end

  def api_get(*args, **opts)
    get_as @_api_user, *args, **opts
  end

  def api_put(*args, **opts)
    _api_do @_api_user, 'put', *args, **opts
  end

  def api_delete(*args, **opts)
    _api_do @_api_user, 'delete', *args, **opts
  end

  def api_post(*args, **opts)
    post_as @_api_user, *args, **opts
  end

  def _set_sign_in_headers(user, opts)
    s = Base64.strict_encode64 [user.email, user.password].join(':')
    opts[:headers] ||= {}
    opts[:headers]['Authorization'] = "Basic #{s}"
  end

  def _api_do(user, method, path, **opts)
    _set_sign_in_headers user, opts if user
    send method, path, **opts
  end
end

module ApiSpec
  def self.included(base)
    base.include SpecDsl
    base.include SimpleJsonapi
    base.include SignIn
    base.extend ClassMethods
  end

  module ClassMethods
    def it_requires_sign_in(method)
      it 'requires sign in' do
        send method
        assert_equal status, 401
      end
    end
  end
end
