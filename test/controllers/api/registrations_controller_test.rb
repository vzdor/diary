require 'test_helper'
require_relative 'api_spec'

class Api::RegistrationsControllerTest < ActionDispatch::IntegrationTest
  include ApiSpec

  def params
    {
      data: {
        type: 'registrations',
        attributes: {
          user: {
            email: 'bob@diary',
            password: 'test',
          },
          diaries: [
            {
              name: 'Test',
              local_id: '1',
              hide_listing: 'no'
            },
            {
              name: 'Secrets',
              local_id: '2',
              hide_listing: 'yes'
            }
          ]
        }
      }
    }
  end

  def create(options = {})
    defaults = {
      params: params,
      as: :json
    }
    post api_registrations_path, defaults.deep_merge(options)
  end

  describe 'POST create' do
    it 'returns 201' do
      create
      assert_equal 201, status
    end

    it 'adds user' do
      create
      user = User.where(email: 'bob@diary').first
      assert user, 'user not found'
    end

    it 'adds diaries' do
      create
      user = User.first
      diaries = user.diaries
      assert_equal 2, diaries.size
    end

    it 'reply in jsonapi format' do
      create
      assert_includes response.parsed_body, 'data'
    end

    it 'returns diaries' do
      create
      attrs = response.parsed_body['data']['attributes']
      assert_includes attrs, 'diaries'
      assert_includes attrs['diaries'][0], 'id'
    end

    describe 'with error' do
      it 'returns error' do
        create params: {
                 data: {
                   attributes: {
                     user: {
                       email: nil
                     }
                   }
                 }
               }
        assert_equal 400, status
        assert_includes response.parsed_body['errors'], 'email'
      end
    end
  end
end
