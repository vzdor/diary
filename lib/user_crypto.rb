require 'openssl'

class UserCrypto
  # How password is generated by the client.
  #
  # The client use pbkdf2 to derive a key from user-typed-password.
  # Then the client generates passwordKey: passwordKey = hmac('2', key)
  # Then he generates the password: password = hmac('0', passwordKey)

  # user_password? must be fast, for session-less API where each
  # request will be authenticated.

  # Returns true if password is user password.
  def self.user_password?(user, password)
    salt = Base64.strict_decode64 user.salt
    salted_password = hmac(salt, password)
    salted_password == Base64.strict_decode64(user.salted_password)
  end

  # Set fields required by user_password?
  def self.signup(user)
    salt = new_salt
    salted_password = hmac(salt, user.password)
    user.salt = Base64.strict_encode64(salt)
    user.salted_password = Base64.strict_encode64(salted_password)
  end

  # Returns 256/8 random bytes.
  def self.new_salt
    OpenSSL::Random.random_bytes 32
  end

  # Returns SHA256 HMAC encrypted string.
  def self.hmac(key, text)
    digest = OpenSSL::Digest::SHA256.new
    OpenSSL::HMAC.digest(digest, key, text)
  end
end
