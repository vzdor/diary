# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies
    * apt-get install postgresql-9.4 libpq-dev

* Configuration
  * Add postgres user

    sudo -u postgres -i bash
    createuser --interactive your_login
    https://wiki.debian.org/PostgreSql#User_access

* Database creation

* Database initialization
  rails db:migrate:reset # To load sql-procedure migrations
  rails db:seed
  RAILS_ENV=test rails db:migrate:reset

* How to run the test suite
  * Minitest

    http://cheat.errtheblog.com/s/minitest

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
