# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170810225406) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"

  create_table "diaries", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.string "local_id", null: false
    t.string "hide_listing", null: false
    t.index ["user_id"], name: "index_diaries_on_user_id"
  end

  create_table "diary_entries", id: :serial, force: :cascade do |t|
    t.text "message", null: false
    t.string "url"
    t.integer "diary_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "local_id", null: false
    t.integer "fingerprints", limit: 2, null: false, array: true
    t.index ["diary_id", "local_id"], name: "index_diary_entries_on_diary_id_and_local_id", unique: true
    t.index ["diary_id"], name: "index_diary_entries_on_diary_id"
  end

  create_table "diary_indices", id: :serial, force: :cascade do |t|
    t.string "local_id", null: false
    t.integer "diary_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "entry_local_ids", default: [], null: false, array: true
    t.string "token", limit: 512, null: false
    t.index ["diary_id", "local_id"], name: "index_diary_indices_on_diary_id_and_local_id", unique: true
    t.index ["diary_id"], name: "index_diary_indices_on_diary_id"
  end

  create_table "index_groups", id: false, force: :cascade do |t|
    t.string "id", null: false
    t.integer "lock_version"
    t.integer "diary_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diary_id", "id"], name: "index_index_groups_on_diary_id_and_id", unique: true
    t.index ["diary_id"], name: "index_index_groups_on_diary_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 256, null: false
    t.string "salt", limit: 512, null: false
    t.string "salted_password", limit: 512, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "diaries", "users"
  add_foreign_key "diary_entries", "diaries"
  add_foreign_key "diary_indices", "diaries"
  add_foreign_key "index_groups", "diaries"
end
