# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# email: bob@diary
# Plain password: test
password = '5Zsj7osO3OsS+UofRvvWfzN6Rih/mPXJztuxtA6iz38='
user = User.new({
  password: password,
  email: 'bob@diary'
})
user.save!
name = '{"encrypted":"DOF9FTu+EYmz3X9REMDzzQ==","iv":"nvRGwbkr0wqn7enjOWw4dQ=="}'
hide_listing = '{"encrypted":"htJv47bTVZMdp2NmQ4/V+A==","iv":"NI9Skcur+Ghv1MTUUmPxXw=="}'
diaries = Diary.create([{name: name, local_id: '1', hide_listing: hide_listing, user: user}])
