class CreateDiaryIndices < ActiveRecord::Migration[5.0]
  def change
    create_table :diary_indices do |t|
      t.string :local_id, null: false
      t.text :content, null: false
      t.references :diary, foreign_key: true, null: false

      t.timestamps
    end

    add_index :diary_indices, [:diary_id, :local_id], unique: true
  end
end
