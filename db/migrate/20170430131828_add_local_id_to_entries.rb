class AddLocalIdToEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_entries, :local_id, :integer, null: false
    add_index :diary_entries, [:diary_id, :local_id], unique: true
  end
end
