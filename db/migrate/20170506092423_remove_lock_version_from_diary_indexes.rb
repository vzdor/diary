class RemoveLockVersionFromDiaryIndexes < ActiveRecord::Migration[5.0]
  def change
    remove_column :diary_indices, :lock_version, :integer
  end
end
