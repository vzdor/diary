class RemoveContentFromDiaryIndices < ActiveRecord::Migration[5.0]
  def change
    remove_column :diary_indices, :content, :string
  end
end
