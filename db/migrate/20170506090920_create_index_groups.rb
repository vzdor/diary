class CreateIndexGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :index_groups, id: false do |t|
      t.string :id, null: false
      t.integer :lock_version
      t.references :diary, foreign_key: true, null: false

      t.timestamps
    end

    add_index :index_groups, [:diary_id, :id], unique: true
  end
end
