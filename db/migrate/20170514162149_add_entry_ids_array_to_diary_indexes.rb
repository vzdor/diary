class AddEntryIdsArrayToDiaryIndexes < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_indices, :entry_ids, :integer, array: true, default: [], null: false
  end
end
