class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :email, limit: 256, null: false
      t.string :salt, limit: 512, null: false
      t.string :salted_password, limit: 512, null: false

      t.timestamps
    end
  end
end
