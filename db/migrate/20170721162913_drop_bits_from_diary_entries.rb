class DropBitsFromDiaryEntries < ActiveRecord::Migration[5.0]
  def change
    remove_column :diary_entries, :bits, 'bit(2016)'
  end
end
