class AddBitsToDiaryEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_entries, :bits, 'bit(2016)', null: false
  end
end
