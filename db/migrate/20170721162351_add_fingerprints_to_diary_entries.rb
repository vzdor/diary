class AddFingerprintsToDiaryEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_entries, :fingerprints, 'smallint[]', null: false
  end
end
