class AddZidxProcedures < ActiveRecord::Migration[5.0]
  def self.up
    sql1 =<<EOT
-- Takes a hash and returns right 28 bits of the hash, a positive integer.
-- If the hash is uniform, then this function is uniform.
CREATE OR REPLACE FUNCTION zidx_integer(bytes bytea) RETURNS integer AS $$
DECLARE
    n integer;
BEGIN
    -- It is the same as taking 7 right characters from a hex string.
    n := ((get_byte(bytes, 28) & 15) << 24) |
          (get_byte(bytes, 29) << 16) |
          (get_byte(bytes, 30) << 8) |
           get_byte(bytes, 31);
    RETURN n;
END;
$$ LANGUAGE plpgsql;
EOT

    execute sql1

    sql2 =<<EOT
CREATE OR REPLACE FUNCTION zidx_includes(doc_id integer, bloomfilter bit(2016), trapdoor bytea[]) RETURNS boolean AS $$
DECLARE
    x bytea; -- i'th trapdoor element
    y bytea; -- i'th codeword element
    bit_position integer; -- position in the bloomfilter.
BEGIN
    FOREACH x IN ARRAY trapdoor
    LOOP
        y := hmac(doc_id::varchar::bytea, x, 'sha256');
        bit_position := mod(zidx_integer(y), 2016);
        IF get_bit(bloomfilter, bit_position) = 0 THEN
           RETURN FALSE;
        END IF;
    END LOOP;
    RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
EOT

    execute sql2
  end

  def self.down
    execute 'DROP FUNCTION zidx_integer(bytea)'
    exeucte 'DROP FUNCTION zidx_includes(integer, bit(2016), bytea[])'
  end
end
