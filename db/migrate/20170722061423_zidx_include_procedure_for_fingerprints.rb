class ZidxIncludeProcedureForFingerprints < ActiveRecord::Migration[5.0]
  def self.up
    sql =<<EOT
CREATE OR REPLACE FUNCTION zidx_includes(doc_id integer, fps smallint[], trapdoor bytea[]) RETURNS boolean AS $$
DECLARE
    x bytea; -- i'th trapdoor element
    y bytea; -- i'th codeword element
    fp smallint; -- fingerprint
    i1 int; -- iis. There is no unsigned integer in SQL, use int.
    i2 int;
    i3 int;
    len int; -- fingerprints array length.
BEGIN
    FOREACH x IN ARRAY trapdoor
    LOOP
        -- Initialize codeword
        y := hmac(doc_id::varchar::bytea, x, 'sha256');
        -- Initialize fingerprint and hashes.
        fp := (get_byte(y, length(y) - 2)::smallint << 8) | get_byte(y, length(y) - 1);
        len := array_length(fps, 1);
        i1 := mod((get_byte(y, 0) << 8) | get_byte(y, 1), len);
        i2 := mod((get_byte(y, 2) << 8) | get_byte(y, 3), len);
        i3 := mod((get_byte(y, 4) << 8) | get_byte(y, 5), len);
        -- Array adressing starts with 1.
        IF fps[i1 + 1] = fp OR fps[i2 + 1] = fp OR fps[i3 + 1] = fp THEN
           RETURN TRUE;
        END IF;
    END LOOP;
    RETURN FALSE;
END;
$$ LANGUAGE plpgsql;
EOT
    execute sql
  end

  def self.down
    execute 'DROP FUNCTION zidx_includes(integer, smallint[], bytea[])'
  end
end
