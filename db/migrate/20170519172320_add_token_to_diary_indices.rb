class AddTokenToDiaryIndices < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_indices, :token, :string, limit: 512, null: false
  end
end
