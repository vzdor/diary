class RenameDiaryIndicesEntryIdsToEntryLocalIds < ActiveRecord::Migration[5.0]
  def change
    rename_column :diary_indices, :entry_ids, :entry_local_ids
  end
end
