class AddLockVersionToDiaryIndexes < ActiveRecord::Migration[5.0]
  def change
    add_column :diary_indices, :lock_version, :integer
  end
end
