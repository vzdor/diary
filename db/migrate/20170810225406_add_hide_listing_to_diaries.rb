class AddHideListingToDiaries < ActiveRecord::Migration[5.1]
  def change
    add_column :diaries, :hide_listing, :string, null: false
  end
end
