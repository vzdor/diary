class CreateDiaryEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :diary_entries do |t|
      t.text :message, null: false
      t.string :url
      t.references :diary, foreign_key: true, null: false

      t.timestamps
    end
  end
end
