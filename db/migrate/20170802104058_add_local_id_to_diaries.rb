class AddLocalIdToDiaries < ActiveRecord::Migration[5.1]
  def change
    add_column :diaries, :local_id, :string, null: false
  end
end
