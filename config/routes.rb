Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    resources :diaries do
      resources :entries, controller: 'diary_entries'
      resources :zqueries, controller: 'zqueries', only: [] do
        post 'all', on: :collection
      end
      resources :localids, controller: 'diary_localids', only: [:create]
    end
    resources :registrations, only: [:create]
  end
end
