Rails.application.config.generators do |g|
  g.assets  false
  g.helper false
  g.stylesheets false
  # Do not generate fixtures
  g.test_framework  :test_unit, fixture: false
end
